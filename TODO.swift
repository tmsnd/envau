
// Bugs:
// Touch Mode: Active/Deactivated
// Longpress: Cancel Overlay Update

// Macro:
// 1. Replace MapKit Annotations? -> Smooth Radius Update
// 2. Replace Settings/Layers Views
// 3. Add SwipeUp Settings

// Micro:
// 0. New Launch Image
// 1. Improve MainUI Animations
// 2. Add Toggle Buttons
// 3. Refine Buttons
