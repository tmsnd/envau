# ENVAU

ENVAU name is a iOS app that enables composition and playback of positional soundscapes

Website: https://abstraqata.com/envau


## Developing
* Install CocoaPods
* Run ```pod install``` in the root directory
* Open Envau.xcworkspace
